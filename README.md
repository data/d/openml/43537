# OpenML dataset: AMD-Stock-Prices-Historical-Data

https://www.openml.org/d/43537

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Advanced Micro Devices, Inc. operates as a semiconductor company worldwide. The company operates in two segments, Computing and Graphics; and Enterprise, Embedded and Semi-Custom. Its products include x86 microprocessors as an accelerated processing unit, chipsets, discrete and integrated graphics processing units (GPUs), data center and professional GPUs, and development services; and server and embedded processors, and semi-custom System-on-Chip (SoC) products, development services, and technology for game consoles. The company provides x86 microprocessors for desktop PCs under the AMD Ryzen, AMD Ryzen PRO, Ryzen, Threadripper, AMD A-Series, AMD FX, AMD Athlon, AMD Athlon PRO, and AMD Pro A-Series processors brands; microprocessors for notebook and 2-in-1s under the AMD Ryzen processors with Radeon Vega GPUs, AMD A-Series, AMD Athlon, AMD Ryzen PRO, and AMD Pro A-Series processors brands; microprocessors for servers under the AMD EPYC and AMD Opteron brands; and chipsets under the AMD trademark. It also offers discrete GPUs for desktop and notebook PCs under the AMD Radeon graphics and AMD Embedded Radeon brands; professional graphics products under the AMD Radeon Pro and AMD FirePro graphics brands; and Radeon Instinct accelerators for servers. In addition, the company provides embedded processor solutions for interactive digital signage, casino gaming, and medical imaging under the AMD Opteron, AMD Athlon, AMD Geode, AMD Ryzen, AMD EPYC, AMD R-Series, and G-Series processors brands; and customer-specific solutions based on AMD CPU, GPU, and multi-media technologies, as well as semi-custom SoC products. It serves original equipment and design manufacturers, datacenters, original design manufacturers, system integrators, independent distributors, online retailers, and add-in-board manufacturers through its direct sales force, independent distributors, and sales representatives. The company was founded in 1969 and is headquartered in Santa Clara, California.
Content
What's inside is more than just rows and columns. Make it easy for others to get started by describing how you acquired the data and what time period it represents, too.
import yfinance as yf  
df = yf.download(tickers="AMD")  
df.to_csv('AMD.csv')

Acknowledgements
This data collected with one line code using yahoo finance and yfinance library.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43537) of an [OpenML dataset](https://www.openml.org/d/43537). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43537/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43537/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43537/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

